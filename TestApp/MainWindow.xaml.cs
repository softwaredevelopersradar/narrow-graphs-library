﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace TestApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            graph.InitializeChart(2,4, 0, 1000, 0, 100, 1000, false, true);
            //graph.OnLineValueUpdate += Graph_OnLineValueUpdate;
            graph.OnThresholdValueUpdate += Graph_OnLineValueUpdate;


            Video.AttackThresholdChanged += Video_ThresholdsChangedEvent;
            Video.PicThresholdChanged += Video_ThresholdsChangedEvent2;

            //FirstSlider.OnValueUpdated += SliderControl_OnValueUpdated;

            antennaControl.AntennaChanged += AntennaControl_AntennaChanged;

            FrequencyAntenna.SettingsChanged += PlsKillMe_AntennaChanged;
        }

        private void PlsKillMe_AntennaChanged(object sender, CuirassGraphsLibrary.FrequencyAntennaSetup e)
        {
            var t = "";
            for (int i = 0; i < e.Antennas.Count; i++)
            {
                t += $"{e.Antennas[i]} ";
            }
            SomeLabel.Text = t;
            SomeLabel3.Text = $"Frequency : {e}";
        }

        private void AntennaControl_AntennaChanged(object sender, IReadOnlyList<int> e)
        {
            var t = "";
            for (int i = 0; i < e.Count; i++)
            {
                t += $"{e[i]} ";
            }
            SomeLabel.Text = t;
        }

        private void Video_ThresholdsChangedEvent2(object sender, int e)
        {
            Dispatcher.Invoke(() =>
            {
                PicLabel.Content = $"Pic threshold level is : {e}";
            });
        }

        private void Video_ThresholdsChangedEvent(object sender, int e)
        {
            Dispatcher.Invoke(()=> 
            {
                AttackLabel.Content = $"Attack threshold level is : {e}";
            });
        }

        private void Graph_OnLineValueUpdate(object sender, EventArgs e)
        {
            Dispatcher?.Invoke(()=> {
                var masterModel = graph.MasterViewModel;
                var slaveModel = graph.SlaveViewModel;
                SomeLabel.Text = $"Master wb : pic {masterModel.WideBandPicThreshold}, attack {masterModel.WideBandAttackThreshold};  " +
                $"Master nb : pic {masterModel.NarrowBandPicThreshold}, attack {masterModel.NarrowBandAttackThreshold}\r\n" +
                $"Slave wb : pic {slaveModel.WideBandPicThreshold}, attack {slaveModel.WideBandAttackThreshold};  " +
                $"Slave nb : pic {slaveModel.NarrowBandPicThreshold}, attack {slaveModel.NarrowBandAttackThreshold}";
            });
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            graph.FillChart(0, GenerateData(1000, 33, 33));
        }

        private static Random god = new Random();

        private double[] GenerateData(int count, int startLevel, int delta)
        {
            var output = new double[count];

            for (int i = 0; i < count; i++)
            {
                output[i] = god.Next(startLevel - delta, startLevel + delta + 1);
            }
            return output;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

            graph.FillChart(1, GenerateData(1000, 66, 22), 100);
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            graph.ClearAllCharts();
        }

        private int signalDirection = 0;

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            directionTimePanorama.AddSignalDirection(2, signalDirection);
            signalDirection += god.Next(0, 20);
            if (signalDirection >= 360)
                signalDirection -= 360;
            directionTimePanorama.AddSignalDirection(1, signalDirection);

            signalDirection += god.Next(0,20);
            if (signalDirection >= 360)
                signalDirection -= 360;
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            directionTimePanorama.DrawChart(2);
        }

        private bool _isEnabled;

        private void ToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            //antennaControl.EnableChanges();
        }

        private void ToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            //antennaControl.DisableChanges();
        }

        private async void Button_Click_5(object sender, RoutedEventArgs e)
        {
            var numberOfCycles = 10;
            var enabledAntennas = antennaControl.GetEnabledAntennas();
            while (numberOfCycles != 0)
            {
                for (int i = 0; i < enabledAntennas.Count; i++)
                {
                    var one = enabledAntennas[i];
                    int two;
                    if (i != enabledAntennas.Count - 1)
                    {
                        two = enabledAntennas[i + 1];
                    }
                    else
                        two = enabledAntennas[0];
                    antennaControl.TurnOn(new List<int>() { one, two });
                    await System.Threading.Tasks.Task.Delay(100);

                }

                //for (int i = 1; i <= 8; i++)
                //{
                //    var one = i;
                //    var two = i + 1;
                //    if (i + 1 == 9)
                //        two = 1;
                //    antennaControl.SetColors(new List<int>() { one, two});
                //    await System.Threading.Tasks.Task.Delay(100);
                //}

                numberOfCycles--;
                //await System.Threading.Tasks.Task.Delay(100);
            }
        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            Bulb.TurnOn();
        }

        private void Button_Click_7(object sender, RoutedEventArgs e)
        {
            Bulb.TurnOff();
        }

        private void Button_Click_8(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            btn.Content = $"Get state : {Bulb.State}";
        }

        private void Button_Click_9(object sender, RoutedEventArgs e)
        {
            //.EnableChanges();
        }

        private void Button_Click_10(object sender, RoutedEventArgs e)
        {
            //Bulb.DisableChanges();
        }

        private void Button_Click_11(object sender, RoutedEventArgs e)
        {
            var videoData = GenerateData(8302, 300, 200);
            Video.DrawVideo(videoData);
        }

        private void Button_Click_12(object sender, RoutedEventArgs e)
        {
            Task.Run(VideoSomeTask);
        }

        private async Task VideoSomeTask() 
        {
            for (int i = 0; i < 100; i++)
            {
                var videoData = GenerateData(8302, 300 + i * 2, 200);
                Dispatcher?.Invoke(()=>
                {
                    Video.DrawVideo(videoData);
                });
                await Task.Delay(100);
            }
        }

        private int id = 0;
        private string GetFileName() 
        {
            switch (id)
            {
                case 0:
                    return "scans1_2.bin";
                case 1:
                    return "scans10_20.bin";
                case 2:
                    return "scans100_200.bin";
                case 3:
                    return "scans1000_2000.bin";
                case 4:
                    return "scans1000_10000.bin";
                default:
                    return "scans.bin";
            }
        }

        private void Button_Click_13(object sender, RoutedEventArgs e)
        {
            var scans = File.ReadAllBytes(GetFileName());
            SomeLabel.Text = GetFileName();
            var numberOfScans = scans.Length / 512;
            var data = new int[numberOfScans];
            byte threshold = 60;
            for (int i = 0; i < scans.Length; i += 512)
            {
                var scan = new byte[512];
                Array.Copy(scans, i, scan, 0, 512);
                var scanMax = scan.Max();
                if (scanMax > threshold)
                {
                    data[i / 512] = scanMax;
                }
            }
            seriesChart.AddData(new int[0], data);
            id++;
            if (id == 5)
                id = 0;
        }

        private void Button_Click_14(object sender, RoutedEventArgs e)
        {
            try 
            {
                Task.Run(async()=>
                {
                    var scans = File.ReadAllBytes("scans.bin");
                    for (int i = 0; i < scans.Length; i += 512)
                    {
                        var scan = new byte[512];
                        Array.Copy(scans, i, scan, 0, 512);
                        Dispatcher?.Invoke(() =>
                        {
                            seriesChart.AddData(new int[0], scan);
                            SomeLabel2.Content = $"Scan number {i / 512}";
                        });
                        await Task.Delay(10);
                    }
                });
            }
            catch 
            { }
        }

        private void SliderControl_OnValueUpdated(object sender, int e)
        {
            MessageBox.Show($"New slider value is {e}");
        }
    }
}
