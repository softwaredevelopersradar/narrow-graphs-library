﻿using System;
using System.Collections.Generic;

namespace TestApp
{
    public static class Simulation
    {
        private static Random god = new Random();

        public static double[] SimulateVideoSignal(int pulseWidthMs, int pulseMs)
        {
            var numberOfPoints = 8192;
            var output = new double[numberOfPoints];

            var numberOfPeaks = 1;
            if (pulseMs <= 100)
                numberOfPeaks = 2;
            if (pulseMs <= 50)
                numberOfPeaks = 3;
            if (pulseMs <= 20)
                numberOfPeaks = 6;

            var peakWidthPoints = pulseWidthMs * 100;
            var peakZeroIndex = 50;
            var peakStartIndecies = new List<int>() { peakZeroIndex };
            if (numberOfPeaks != 1)
            {
                var delta = pulseMs * 70;
                for (int i = 1; i < numberOfPeaks; i++)
                {
                    peakStartIndecies.Add(peakZeroIndex + delta * i);
                }
            }

            //fill

            var noiseLevel = 160;
            var noiseDelta = 20;
            var peakLevel = 580;
            var peakDelta = 5;

            for (int i = 0; i < output.Length; i++)
            {
                if (peakStartIndecies.Contains(i))
                {
                    for (int j = 0; j < peakWidthPoints; j++, i++)
                    {
                        output[i] = god.Next(peakLevel - peakDelta, peakLevel + peakDelta + 1);
                    }
                    i--;
                    continue;
                }
                else
                    output[i] = god.Next(noiseLevel - noiseDelta, noiseLevel + noiseDelta + 1);
            }

            return output;
        }

        public static double[] SimulateRfSignalsWithoutWindow(int deltaFreqMhz)
        {
            var numberOfPoints = 1024;
            var output = new double[numberOfPoints];

            var maxLevel = 6;
            if (deltaFreqMhz <= 5)
                maxLevel = 20;
            if (deltaFreqMhz <= 3)
                maxLevel = 80;
            if (deltaFreqMhz <= 1)
                maxLevel = 100;

            for (int i = 0; i < output.Length; i++)
            {
                output[i] = god.Next(-maxLevel, maxLevel + 1);
            }

            return output;
        }

        public static double[] SimulateRfSignalsWithWindow(int deltaFreqMhz)
        {
            var numberOfPoints = 1024;
            var output = new double[numberOfPoints];

            var maxLevel = 6;
            double multiplier = 10;
            if (deltaFreqMhz <= 5)
            {
                maxLevel = 20;
                multiplier = 3;
            }
            if (deltaFreqMhz <= 3)
            {
                maxLevel = 80;
                multiplier = 1.5;
            }
            if (deltaFreqMhz <= 1)
            {
                maxLevel = 100;
                multiplier = 1.5;
            }
            var delta = Math.PI / (numberOfPoints - numberOfPoints * 2 / 10);
            var superDelta = 50;
            var sideLevel = 2;
            for (int i = 0; i < output.Length; i++)
            {
                if (i < numberOfPoints / 10 || i >= numberOfPoints - numberOfPoints / 10)
                {
                    output[i] = god.Next(-sideLevel, sideLevel + 1);
                    continue;
                }
                var sign = i % 2 == 0 ? 1 : -1;
                output[i] = Math.Sin(delta * (i - numberOfPoints / 10)) * god.Next(maxLevel - superDelta, maxLevel + superDelta + 1) * sign / multiplier;
            }

            return output;
        }
    }
}
