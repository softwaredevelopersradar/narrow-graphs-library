﻿using Arction.Wpf.Charting;
using Arction.Wpf.Charting.Axes;
using Arction.Wpf.Charting.SeriesXY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CuirassGraphsLibrary
{
    /// <summary>
    /// Interaction logic for SeriesChart.xaml
    /// </summary>
    public partial class SeriesChart : UserControl
    {
        private readonly static Color[] LineColors = new Color[]
        {
            Colors.LightYellow, Colors.LightGreen, Colors.LightSkyBlue,
        };

        private readonly int _minX = 0;
        private readonly int _maxX = 8302;
        private readonly int _minY = -500;
        private readonly int _maxY = 1500;

        public SeriesChart()
        {
            InitializeComponent();
            SetDeploymentKey();
            InitializeChart();
            InitializeGraphs();
        }

        private void SetDeploymentKey()
        {
            var deploymentKey = "lgCAALq+gYUUUNQBJABVcGRhdGVhYmxlVGlsbD0yMDE5LTA5LTE5I1JldmlzaW9uPTACgD8RLUj4MtJqUZJYP08cAT2iWwf3J9OtBpdL7i2N+0kh7SWkWVA97OxGhM4wObsk67coGddfPr0up6PC3C0KPwwMCiXxkTBWdZ08iYj+WZzzt0Nh0WCA1IHun718ZKUQZyIfbWo+Zm/ye5a/SYJRwoenYVg95HKI3lUD+tAs9E5lNkeIgHiWrxUpQCFd+lN3d6SVnmaaRwMoLdT6iZF8bbI9drqJKFlcQX2RmV5CHt9ABh2AS4G8AbJMUHJrk4dxbyYxZwINYnlhMJPLtitabW+iK+ZQel62jiAm7jPlCGuWUEX34UUVZkOov5jUOIfYuIlgeDLbVNrcdWXOyy34b/+/D5WBCfBrbbINSOFtti+asDiZzEK6nbPL4FU90A14EOFoY68fsAkM3mdae2V0Kn42zZ0imxz84lfwsYfgjDO7MLwKmeU8YSJQJgRTL4A7bJl72NmDGlBnoPCOvDiTOe3xzEKwBle0yNbQfvvOJ47mwibGxo0gxv/o78RSq0SaaMs=";
            LightningChartUltimate.SetDeploymentKey(deploymentKey);
        }

        private void InitializeChart()
        {
            var xAxis = chart.ViewXY.XAxes[0];
            var yAxis = chart.ViewXY.YAxes[0];
            xAxis.SetRange(_minX, _maxX);
            yAxis.SetRange(_minY, _maxY);

            chart.Title.Visible = false;
            xAxis.Title.Visible = false;
            yAxis.Title.Visible = false;

            xAxis.RangeChanged += XAxeRangeChanged;
            yAxis.RangeChanged += YAxeRangeChanged;

            xAxis.AxisColor = Color.FromArgb(0xFF, 0x97, 0xD8, 0xDE);
            yAxis.AxisColor = Color.FromArgb(0xFF, 0x97, 0xD8, 0xDE);
            xAxis.AxisThickness = 1;
            yAxis.AxisThickness = 1;
            chart.ViewXY.Border.Color = Colors.Transparent;

            xAxis.MouseScaling = false;
            yAxis.MouseScaling = false;
        }

        private void XAxeRangeChanged(object sender, RangeChangedEventArgs e)
        {
            SetAxisNewRange(e, _minX, _maxX);
        }

        private void YAxeRangeChanged(object sender, RangeChangedEventArgs e)
        {
            SetAxisNewRange(e, _minY, _maxY);
        }

        private void SetAxisNewRange(RangeChangedEventArgs axisEventArgs, int minValue, int maxValue)
        {
            if (axisEventArgs.Axis.Maximum == maxValue && axisEventArgs.Axis.Minimum == minValue)
                return;
            if (axisEventArgs.NewMax > maxValue)
                axisEventArgs.NewMax = maxValue;
            if (axisEventArgs.NewMin < minValue)
                axisEventArgs.NewMin = minValue;
            axisEventArgs.Axis.SetRange(axisEventArgs.NewMin, axisEventArgs.NewMax);
        }

        private void InitializeGraphs()
        {
            var polyLine = new PointLineSeries()
            {
                MouseInteraction = false,
                ShowInLegendBox = false
            };
            polyLine.LineStyle.Color = LineColors[0];
            chart.ViewXY.PointLineSeries.Add(polyLine);
            polyLine.PointsVisible = true;

            var line = new BarSeries();
            line.Fill.Color = Colors.Orange;
            line.Fill.GradientFill = GradientFill.Solid;
            line.BorderColor = Colors.Transparent;
            chart.ViewXY.BarSeries.Add(line);
            line.BarThickness = 5;
            var line2 = new BarSeries();
            line2.Fill.Color = Colors.Yellow;
            line2.BarThickness = 2;
            line2.BaseLevel = 0;
            chart.ViewXY.BarSeries.Add(line2);
        }

        public void AddData(int[] masterLevels, byte[] slaveLevels) 
        {
            chart.BeginUpdate();
            var line2 = chart.ViewXY.PointLineSeries[0];
            var slaveValues = new SeriesPoint[slaveLevels.Length];
            for (int i = 0; i < slaveLevels.Length; i++)
            {
                slaveValues[i] = new SeriesPoint(i + 1, slaveLevels[i]);
            }
            line2.Points = slaveValues;
            chart.EndUpdate();
        }

        public void AddData(int[] masterLevels, int[] slaveLevels)
        {
            chart.BeginUpdate();
            var line2 = chart.ViewXY.PointLineSeries[0];
            var slaveValues = new SeriesPoint[slaveLevels.Length];
            for (int i = 0; i < slaveLevels.Length; i++)
            {
                slaveValues[i] = new SeriesPoint(i + 1, slaveLevels[i]);
            }
            line2.Points = slaveValues;
            chart.EndUpdate();
        }
    }
}
