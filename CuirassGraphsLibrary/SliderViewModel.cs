﻿using System.ComponentModel;

namespace CuirassGraphsLibrary
{
    public class SliderViewModel : INotifyPropertyChanged
    {
        private double _value = 0;
        public double Value
        {
            get => _value;
            set
            {
                if (_value == value)
                    return;
                _value = value;
                OnPropertyChanged(nameof(Value));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public SliderViewModel() 
        {
            OnPropertyChanged(nameof(Value));
        }
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
