﻿using System.Collections.Generic;

namespace CuirassGraphsLibrary
{
    public struct FrequencyAntennaSetup
    {
        public int FrequencyMhz { get; }
        public IReadOnlyList<int> Antennas { get; }
        public FrequencyAntennaSetup(int frequencyMhz, IReadOnlyList<int> antennas) 
        {
            FrequencyMhz = frequencyMhz;
            Antennas = antennas;
        }
    }
}
