﻿using Arction.Wpf.Charting;
using Arction.Wpf.Charting.Axes;
using Arction.Wpf.Charting.SeriesXY;
using System;
using System.Linq;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Controls;
using System.Windows;

namespace CuirassGraphsLibrary
{
    /// <summary>
    /// Interaction logic for VideoGraphControl.xaml
    /// </summary>
    public partial class VideoGraphControl : UserControl
    {
        private readonly static Color[] LineColors = new Color[]
        {
            Colors.LightYellow, Colors.LightGreen, Colors.LightSkyBlue,
        };

        private readonly VideoGraphModel _model;
        private DeviceThresholds Thresholds =>
            _model.Device == FtmDevice.Master
            ? _model.MasterThresholds
            : _model.SlaveThresholds;

        private VideoThresholds ActiveThresholds =>
            _model.Mode == BandMode.Wideband
            ? Thresholds.WideThresholds
            : Thresholds.NarrowThresholds;

        private PointLineSeries VideoLine => chart.ViewXY.PointLineSeries[0];
        private ConstantLine AttackLine => chart.ViewXY.ConstantLines[0];
        private ConstantLine PicLine => chart.ViewXY.ConstantLines[1]; 
        
        private readonly int _minX = 0;
        private readonly int _maxX = 8302;
        private readonly int _minY = -500;
        private readonly int _maxY = 1500;

        private bool attackLineChanging = false;
        private bool picLineChanging = false;
        private double lastPicValue = 0;

        public VideoGraphModel Model => (VideoGraphModel)_model.Clone();

        public event EventHandler<int> AttackThresholdChanged;
        public event EventHandler<int> PicThresholdChanged;
        public event EventHandler<BandMode> VideoModeChanged;

        public VideoGraphControl()
        {
            InitializeComponent();
            SetDeploymentKey();
            InitializeChart();
            InitializeGraphs();

            _model = new VideoGraphModel();

            
        }

        private void SetDeploymentKey()
        {
            var deploymentKey = "lgCAALq+gYUUUNQBJABVcGRhdGVhYmxlVGlsbD0yMDE5LTA5LTE5I1JldmlzaW9uPTACgD8RLUj4MtJqUZJYP08cAT2iWwf3J9OtBpdL7i2N+0kh7SWkWVA97OxGhM4wObsk67coGddfPr0up6PC3C0KPwwMCiXxkTBWdZ08iYj+WZzzt0Nh0WCA1IHun718ZKUQZyIfbWo+Zm/ye5a/SYJRwoenYVg95HKI3lUD+tAs9E5lNkeIgHiWrxUpQCFd+lN3d6SVnmaaRwMoLdT6iZF8bbI9drqJKFlcQX2RmV5CHt9ABh2AS4G8AbJMUHJrk4dxbyYxZwINYnlhMJPLtitabW+iK+ZQel62jiAm7jPlCGuWUEX34UUVZkOov5jUOIfYuIlgeDLbVNrcdWXOyy34b/+/D5WBCfBrbbINSOFtti+asDiZzEK6nbPL4FU90A14EOFoY68fsAkM3mdae2V0Kn42zZ0imxz84lfwsYfgjDO7MLwKmeU8YSJQJgRTL4A7bJl72NmDGlBnoPCOvDiTOe3xzEKwBle0yNbQfvvOJ47mwibGxo0gxv/o78RSq0SaaMs=";
            LightningChartUltimate.SetDeploymentKey(deploymentKey);
        }

        private void InitializeChart()
        {
            var xAxis = chart.ViewXY.XAxes[0];
            var yAxis = chart.ViewXY.YAxes[0];
            xAxis.SetRange(_minX, _maxX);
            yAxis.SetRange(_minY, _maxY);

            chart.Title.Visible = false;
            xAxis.Title.Visible = false;
            yAxis.Title.Visible = false;

            xAxis.RangeChanged += XAxeRangeChanged;
            yAxis.RangeChanged += YAxeRangeChanged;

            xAxis.AxisColor = Color.FromArgb(0xFF, 0x97, 0xD8, 0xDE);
            yAxis.AxisColor = Color.FromArgb(0xFF, 0x97, 0xD8, 0xDE);
            xAxis.AxisThickness = 1;
            yAxis.AxisThickness = 1;
            chart.ViewXY.Border.Color = Colors.Transparent;

            xAxis.MouseScaling = false;
            yAxis.MouseScaling = false;
        }

        private void XAxeRangeChanged(object sender, RangeChangedEventArgs e)
        {
            SetAxisNewRange(e, _minX, _maxX);
        }

        private void YAxeRangeChanged(object sender, RangeChangedEventArgs e)
        {
            SetAxisNewRange(e, _minY, _maxY);
        }

        private void SetAxisNewRange(RangeChangedEventArgs axisEventArgs, int minValue, int maxValue) 
        {
            if (axisEventArgs.Axis.Maximum == maxValue && axisEventArgs.Axis.Minimum == minValue)
                return;
            if (axisEventArgs.NewMax > maxValue)
                axisEventArgs.NewMax = maxValue;
            if (axisEventArgs.NewMin < minValue)
                axisEventArgs.NewMin = minValue;
            axisEventArgs.Axis.SetRange(axisEventArgs.NewMin, axisEventArgs.NewMax);
        }

        private void InitializeGraphs()
        {
            var videoLine = new PointLineSeries()
            {
                MouseInteraction = false,
                ShowInLegendBox = false
            };
            videoLine.LineStyle.Color = LineColors[0];
            chart.ViewXY.PointLineSeries.Add(videoLine);

            var attackLine = new ConstantLine();
            attackLine.LineStyle.Width = 3;
            attackLine.LineStyle.Color = LineColors[1];
            attackLine.Title.AutoPlacement = true;
            attackLine.Title.Text = AttackTitle;
            attackLine.MouseDown += AttackLine_MouseDown;
            chart.ViewXY.ConstantLines.Add(attackLine);


            var picLine = new ConstantLine();
            picLine.LineStyle.Width = 3;
            picLine.LineStyle.Color = LineColors[2];
            picLine.Title.AutoPlacement = true;
            picLine.Title.Text = PicTitle;
            picLine.MouseDown += PicLine_MouseDown;
            chart.ViewXY.ConstantLines.Add(picLine);

            chart.MouseUp += Chart_MouseUp;
        }

        private void AttackLine_MouseDown(object sender, MouseEventArgs e)
        {
            attackLineChanging = true;
        }

        private void PicLine_MouseDown(object sender, MouseEventArgs e)
        {
            picLineChanging = true;
            lastPicValue = PicLine.Value;
        }

        private void Chart_MouseUp(object sender, MouseEventArgs e)
        {
            if (attackLineChanging)
            {
                ActiveThresholds.AttackThreshold = (int)AttackLine.Value;
                AttackThresholdChanged?.Invoke(this, ActiveThresholds.AttackThreshold);
                attackLineChanging = false;
                if (AttackLine.Value < 1)
                    AttackLine.Value = ActiveThresholds.AttackThreshold;
                return;
            }

            if (picLineChanging)
            {
                var diff = lastPicValue - PicLine.Value;
                ActiveThresholds.PicThreshold += (int)diff;
                picLineChanging = false;
                PicThresholdChanged?.Invoke(this, ActiveThresholds.PicThreshold);
            }
        }

        public void DrawVideo(double[] videoData) 
        {
            if (videoData.Length == 0)
            {
                ClearChart();
                return;
            }

            chart.BeginUpdate();
            
            VideoLine.Points = new SeriesPoint[videoData.Length];
            for (int i = 0; i < videoData.Length; i++)
            {
                if (videoData[i] > 1000)
                {
                    videoData[i] %= 1000;
                    videoData[i] /= 2;
                    videoData[i] *= -1;
                }
                VideoLine.Points[i] = new SeriesPoint(i, videoData[i]);
            }
            var maxVideoValue = videoData.Max();
            //DrawPicLine(maxVideoValue);

            chart.EndUpdate();
        }

        public void ClearChart()
        {
            chart.BeginUpdate();
            VideoLine.Points = new SeriesPoint[0];
            // we do not clear pic and attack line values
            chart.EndUpdate();
        }

        private void DrawPicLine(double videoMaxLevel) 
        {
            if (picLineChanging)
                return;
            chart.BeginUpdate();
            PicLine.Value = videoMaxLevel - ActiveThresholds.PicThreshold;
            chart.EndUpdate();
        }

        public void SetMasterThresholds(int wideAttackThreshold, int widePicThreshold, int narrowAttackThreshold, int narrowPicThreshold) 
        {
            _model.MasterThresholds.WideThresholds.AttackThreshold = wideAttackThreshold;
            _model.MasterThresholds.WideThresholds.PicThreshold = widePicThreshold;
            _model.MasterThresholds.NarrowThresholds.AttackThreshold = narrowAttackThreshold;
            _model.MasterThresholds.NarrowThresholds.PicThreshold = narrowPicThreshold;

            SetAttackThreshold();
        }

        public void SetSlaveThresholds(int wideAttackThreshold, int widePicThreshold, int narrowAttackThreshold, int narrowPicThreshold)
        {
            _model.SlaveThresholds.WideThresholds.AttackThreshold = wideAttackThreshold;
            _model.SlaveThresholds.WideThresholds.PicThreshold = widePicThreshold;
            _model.SlaveThresholds.NarrowThresholds.AttackThreshold = narrowAttackThreshold;
            _model.SlaveThresholds.NarrowThresholds.PicThreshold = narrowPicThreshold;

            SetAttackThreshold();
        }

        public void SetThreshold(bool isMaster, bool isWide, bool isAttack, int threshold) 
        {
            var deviceThreshold = isMaster ? _model.MasterThresholds : _model.SlaveThresholds;
            var videoThresholds = isWide ? deviceThreshold.WideThresholds : deviceThreshold.NarrowThresholds;
            if (isAttack) 
                videoThresholds.AttackThreshold = threshold;
            else 
                videoThresholds.PicThreshold = threshold;

            SetAttackThreshold();
        }

        private void SetAttackThreshold() 
        {
            AttackLine.Value = ActiveThresholds.AttackThreshold;
        }

        private void DeviceButton_Click(object sender, RoutedEventArgs e)
        {
            _model.Device = _model.Device != FtmDevice.Master 
                ? FtmDevice.Master
                : FtmDevice.Slave;

            DeviceButton.Content = _model.Device == FtmDevice.Master ? "M" : "S" ;
            SetAttackThreshold();
        }

        private void ModeButton_Click(object sender, RoutedEventArgs e)
        {
            _model.Mode = _model.Mode != BandMode.Wideband
                ? BandMode.Wideband
                : BandMode.Narrowband;

            ModeButton.Content = _model.Mode == BandMode.Wideband ? "W" : "N";
            SetAttackThreshold();
            VideoModeChanged?.Invoke(this, _model.Mode);
        }

        public string PicTitle
        {
            get => (string)GetValue(PicTitleProperty);
            set 
            { 
                SetValue(PicTitleProperty, value);
            }
        }

        public static readonly DependencyProperty PicTitleProperty = DependencyProperty.Register(
            "PicTitle", typeof(string), typeof(VideoGraphControl), new FrameworkPropertyMetadata("Pic threshold", FrameworkPropertyMetadataOptions.AffectsRender, ThresholdTitleCallback));

        public string AttackTitle
        {
            get => (string)GetValue(AttackTitleProperty);
            set 
            { 
                SetValue(AttackTitleProperty, value);
            }
        }

        public static readonly DependencyProperty AttackTitleProperty = DependencyProperty.Register(
            "AttackTitle", typeof(string), typeof(VideoGraphControl), new FrameworkPropertyMetadata("Attack threshold", FrameworkPropertyMetadataOptions.AffectsRender, ThresholdTitleCallback));

        private static void AttackThresholdCallback(DependencyObject dependancyObject, DependencyPropertyChangedEventArgs args)
        {
            try
            {
                var videoGraphControl = dependancyObject as VideoGraphControl;
                videoGraphControl.EnsurePicThresholdText();
            }
            catch { }
        }

        private static void ThresholdTitleCallback(DependencyObject dependancyObject, DependencyPropertyChangedEventArgs args) 
        {
            try
            {
                var videoGraphControl = dependancyObject as VideoGraphControl;
                if (args.Property.Name.Contains("Pic"))
                {
                    videoGraphControl.EnsurePicThresholdText();
                }
                else 
                {
                    videoGraphControl.EnsureAttackThresholdText();
                }
            }
            catch { }
        }

        private void EnsureAttackThresholdText()
        {
            if (AttackLine.Title.Text.Equals(AttackTitle)) return;
            AttackLine.Title.Text = AttackTitle;
        }

        private void EnsurePicThresholdText() 
        {
            if (PicLine.Title.Text.Equals(PicTitle)) return;
            PicLine.Title.Text = PicTitle;
        }
    }
}
