﻿using System.Windows.Controls;
using System.Windows.Media;
using Arction.Wpf.Charting;
using Arction.Wpf.Charting.SeriesXY;
using Arction.Wpf.Charting.Axes;
using System;
using System.Windows;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace CuirassGraphsLibrary
{
    using System.Threading;

    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class NarrowGraph : UserControl
    {
        private int _minX = 0;
        private int _maxX = 10;
        private int _minY = 0;
        private int _maxY = 10;
        private int _numberOfPoints = 10;

        public double Th
        {
            get => chart.ViewXY.ConstantLines[0].Value;
            set
            {
                chart.ViewXY.ConstantLines[0].Value = value;
                ThresholdChanged?.Invoke(this, value);
            }
        }
        public ThresholdViewModel MasterViewModel { get; }
        public ThresholdViewModel SlaveViewModel { get; }
        public bool IsWideBandMode { get; private set; } = true;

        public event EventHandler OnVideoWorkingBandChanged;
        public event EventHandler<bool> MeasureModeChanged;
        public event EventHandler<double> ThresholdChanged;

        public NarrowGraph()
        {
            InitializeComponent();
            var deploymentKey = "lgCAALq+gYUUUNQBJABVcGRhdGVhYmxlVGlsbD0yMDE5LTA5LTE5I1JldmlzaW9uPTACgD8RLUj4MtJqUZJYP08cAT2iWwf3J9OtBpdL7i2N+0kh7SWkWVA97OxGhM4wObsk67coGddfPr0up6PC3C0KPwwMCiXxkTBWdZ08iYj+WZzzt0Nh0WCA1IHun718ZKUQZyIfbWo+Zm/ye5a/SYJRwoenYVg95HKI3lUD+tAs9E5lNkeIgHiWrxUpQCFd+lN3d6SVnmaaRwMoLdT6iZF8bbI9drqJKFlcQX2RmV5CHt9ABh2AS4G8AbJMUHJrk4dxbyYxZwINYnlhMJPLtitabW+iK+ZQel62jiAm7jPlCGuWUEX34UUVZkOov5jUOIfYuIlgeDLbVNrcdWXOyy34b/+/D5WBCfBrbbINSOFtti+asDiZzEK6nbPL4FU90A14EOFoY68fsAkM3mdae2V0Kn42zZ0imxz84lfwsYfgjDO7MLwKmeU8YSJQJgRTL4A7bJl72NmDGlBnoPCOvDiTOe3xzEKwBle0yNbQfvvOJ47mwibGxo0gxv/o78RSq0SaaMs=";
            LightningChartUltimate.SetDeploymentKey(deploymentKey);
            MasterViewModel = new ThresholdViewModel();
            SlaveViewModel = new ThresholdViewModel();

        }

        protected readonly static Color[] Colorz = new Color[]
                                                       {
                                                           Colors.DarkOrange, Colors.WhiteSmoke,  Colors.Gold,
                                                           Colors.DeepPink, Colors.LightSkyBlue, Colors.LightPink,
                                                           Color.FromRgb(0xe0, 0xff, 0xb4), Colors.LightCyan,
                                                           Colors.LightYellow, Color.FromRgb(0xc3, 0xdf, 0xfa)
                                                       };

        public void InitializeChart(int numberOfCharts, int numberOfConstantLines, int minX, int maxX, int minY, int maxY, int numberOfPoints,
            bool isLegendVisible = false, bool IsWideNarrowGraph = false, bool pointsVisible = false)
        {
            for (int i = 0; i < numberOfCharts && i < Colorz.Length; i++)
            {
                var pls = new PointLineSeries();
                pls.LineStyle.Color = Colorz[i];
                pls.PointsVisible = pointsVisible;
                pls.MouseInteraction = false;
                switch (i)
                {
                    case 0:
                        pls.Title.Text = "Master";
                        break;
                    case 1:
                        pls.Title.Text = "Slave";
                        break;
                }
                chart.ViewXY.PointLineSeries.Add(pls);
                maxDictionary.Add(i, -1);
            }

            yo = new Dictionary<int, double>(numberOfConstantLines);
            w = new Dictionary<int, Stopwatch>(numberOfConstantLines);
            //for (int i = 0; i < numberOfConstantLines && i < Colorz.Length; i++)
            //{
            //    var cl = new ConstantLine();
            //    cl.LineStyle.Width = 3;
            //    cl.LineStyle.Color = Colorz[i + numberOfCharts];
            //    //cl.ValueChanged += ConstantLine_ValueChanged;
            //    cl.MouseInteraction = true;
            //    cl.Tag = i;
            //    cl.Title.AutoPlacement = true;
            //    switch (i)
            //    {
            //        case 0:
            //            cl.Title.Text = "Master pic";
            //            break;
            //        case 1:
            //            cl.Title.Text = "Master attack";
            //            break;
            //        case 2:
            //            cl.Title.Text = "Slave pic";
            //            break;
            //        case 3:
            //            cl.Title.Text = "Slave attack";
            //            break;
            //    }
            //    chart.ViewXY.ConstantLines.Add(cl);

            //    yo.Add(i, -1);
            //    w.Add(i, new Stopwatch());
            //}
            //if (numberOfConstantLines != 0)
            //{
            //    chart.ViewXY.ConstantLines[3].Visible = false;
            //    chart.ViewXY.ConstantLines[2].Visible = false;
            //}
            //Task.Run(NewValueTask);

            var constantLine = new ConstantLine();
            constantLine.LineStyle.Width = 2;
            constantLine.LineStyle.Color = Colors.BlueViolet;
            //cl.ValueChanged += ConstantLine_ValueChanged;
            constantLine.MouseInteraction = true;
            constantLine.Tag = 1;
            constantLine.Title.AutoPlacement = true;
            constantLine.Title.Text = "Th";
            constantLine.ValueChanged += Cl_ValueChanged;

            chart.ViewXY.ConstantLines.Add(constantLine);



            if (!isLegendVisible)
                foreach (var box in chart.ViewXY.LegendBoxes)
                    box.Visible = false;

            _minX = minX;
            _maxX = maxX;
            _minY = minY;
            _maxY = maxY;
            _numberOfPoints = numberOfPoints;

            chart.ViewXY.XAxes[0].SetRange(_minX, _maxX);
            chart.ViewXY.XAxes[0].FormatValueLabel += (sender, args) => { return Math.Abs(args.Value).ToString(); };
            chart.ViewXY.YAxes[0].SetRange(_minY, _maxY);

            chart.Title.Visible = false;
            chart.ViewXY.XAxes[0].Title.Visible = false;
            chart.ViewXY.YAxes[0].Title.Visible = false;

            chart.ViewXY.XAxes[0].RangeChanged += XAxeRangeChanged;
            chart.ViewXY.YAxes[0].RangeChanged += YAxeRangeChanged;

            if (!IsWideNarrowGraph)
            {
                WideNarrowButton.Visibility = Visibility.Collapsed;
                LegendBoxToggleButton.Visibility = Visibility.Collapsed;
            }
            else
            {
                WideNarrowButton.Visibility = Visibility.Visible;
                LegendBoxToggleButton.Visibility = Visibility.Visible;
            }

            chart.ViewXY.XAxes[0].AxisColor = Color.FromArgb(0xFF, 0x97, 0xD8, 0xDE);
            chart.ViewXY.YAxes[0].AxisColor = Color.FromArgb(0xFF, 0x97, 0xD8, 0xDE);
            chart.ViewXY.XAxes[0].AxisThickness = 1;
            chart.ViewXY.YAxes[0].AxisThickness = 1;
            chart.ViewXY.Border.Color = Colors.Transparent;

            chart.ViewXY.YAxes[0].MouseScaling = false;
            chart.ViewXY.XAxes[0].MouseScaling = false;

            Th = 30;
        }

        private void Cl_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            Th = e.NewValue;
        }

        private void ConstantLine_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            yo[(int)e.ConstantLine.Tag] = e.NewValue;
            w[(int)e.ConstantLine.Tag].Restart();
        }
        public event EventHandler OnThresholdValueUpdate;
        private Dictionary<int, double> yo;
        private Dictionary<int, Stopwatch> w;
        private async Task NewValueTask()
        {
            while (true)
            {
                try
                {
                    foreach (var watch in w)
                    {
                        if (yo[watch.Key] == -1)
                            continue;
                        if (watch.Value.Elapsed.TotalMilliseconds > 300)
                        {
                            switch (watch.Key)
                            {
                                case 0:
                                    if (IsWideBandMode)
                                        MasterViewModel.WideBandPicThreshold = (int)yo[watch.Key];
                                    else
                                        MasterViewModel.NarrowBandPicThreshold = (int)yo[watch.Key];
                                    break;
                                case 1:
                                    if (IsWideBandMode)
                                        MasterViewModel.WideBandAttackThreshold = (int)yo[watch.Key];
                                    else
                                        MasterViewModel.NarrowBandAttackThreshold = (int)yo[watch.Key];
                                    break;
                                case 2:
                                    if (IsWideBandMode)
                                        SlaveViewModel.WideBandPicThreshold = (int)yo[watch.Key];
                                    else
                                        SlaveViewModel.NarrowBandPicThreshold = (int)yo[watch.Key];
                                    break;
                                case 3:
                                    if (IsWideBandMode)
                                        SlaveViewModel.WideBandAttackThreshold = (int)yo[watch.Key];
                                    else
                                        SlaveViewModel.NarrowBandAttackThreshold = (int)yo[watch.Key];
                                    break;
                            }
                            OnThresholdValueUpdate?.Invoke(this, EventArgs.Empty);
                            watch.Value.Stop();
                            yo[watch.Key] = -1;
                        }
                    }
                    await Task.Delay(50);
                }
                catch { }
            }
        }

        public void SetThresholdValue(bool isMaster, bool isWideBand, bool isPic, int value)
        {
            var model = isMaster
                ? MasterViewModel
                : SlaveViewModel;
            var indexAddedValue = isMaster ? 0 : 2;
            if (isWideBand)
            {
                if (isPic)
                {
                    //model.WideBandPicThreshold = value;
                    SetConstantLineValue(0 + indexAddedValue, value);
                }
                else
                {
                    //model.WideBandAttackThreshold = value;
                    SetConstantLineValue(1 + indexAddedValue, value);
                }
            }
            else
            {
                if (isPic)
                {
                    //model.NarrowBandPicThreshold = value;
                    SetConstantLineValue(0 + indexAddedValue, value);
                }
                else
                {
                    //model.NarrowBandAttackThreshold = value;
                    SetConstantLineValue(1 + indexAddedValue, value);
                }
            }
        }
        private void SetConstantLineValue(int constantLineId, int constantLineValue)
        {
            var cl = chart.ViewXY.ConstantLines[constantLineId];
            //var modelValue = GetModelValue(constantLineId);
            //if (modelValue == constantLineValue)
            //    return;
            chart.BeginUpdate();
            GetModelValue(constantLineId);
            if (constantLineId % 2 != 0)
                cl.Value = constantLineValue;
            //cl.Value = constantLineValue;
            chart.EndUpdate();

            void GetModelValue(int id)
            {
                switch (id)
                {
                    case 0:
                        if (IsWideBandMode)
                            MasterViewModel.WideBandPicThreshold = constantLineValue;
                        else
                            MasterViewModel.NarrowBandPicThreshold = constantLineValue;
                        break;
                    case 1:
                        if (IsWideBandMode)
                            MasterViewModel.WideBandAttackThreshold = constantLineValue;
                        else
                            MasterViewModel.NarrowBandAttackThreshold = constantLineValue;
                        break;
                    case 2:
                        if (IsWideBandMode)
                            SlaveViewModel.WideBandPicThreshold = constantLineValue;
                        else
                            SlaveViewModel.NarrowBandPicThreshold = constantLineValue;
                        break;
                    case 3:
                        if (IsWideBandMode)
                            SlaveViewModel.WideBandAttackThreshold = constantLineValue;
                        else
                            SlaveViewModel.NarrowBandAttackThreshold = constantLineValue;
                        break;
                }
            }
        }

        private void XAxeRangeChanged(object sender, RangeChangedEventArgs e)
        {
            if (e.Axis.Maximum == _maxX && e.Axis.Minimum == _minX)
                return;
            if (e.NewMax > _maxX)
                e.NewMax = _maxX;
            if (e.NewMin < _minX)
                e.NewMin = _minX;
            e.Axis.SetRange(e.NewMin, e.NewMax);
        }

        private void YAxeRangeChanged(object sender, RangeChangedEventArgs e)
        {
            if (e.Axis.Maximum == _maxY && e.Axis.Minimum == _minY)
                return;
            if (e.NewMax > _maxY)
                e.NewMax = _maxY;
            if (e.NewMin < _minY)
                e.NewMin = _minY;
            e.Axis.SetRange(e.NewMin, e.NewMax);
        }

        private readonly Dictionary<int, double> maxDictionary = new Dictionary<int, double>();

        public void FillChart(int chartIndex, double[] data, int offset = 0)
        {
            if (data.Length == 0)
            {
                ClearChart(chartIndex);
                return;
            }
            if (chartIndex > chart.ViewXY.PointLineSeries.Count ||
                data.Length > _numberOfPoints)
                return;

            chart.BeginUpdate();
            var pls = chart.ViewXY.PointLineSeries[chartIndex];
            pls.Points = new SeriesPoint[data.Length];
            object obj = new object();
            //Parallel.For(
            //    0,
            //    data.Length,
            //    index =>
            //        {
            //            if (data[index] > 1000)
            //            {
            //                data[index] %= 1000;
            //                data[index] /= 2;
            //                data[index] *= -1;
            //            }
            //            lock (obj)
            //            {
            //                pls.Points[index] = new SeriesPoint(index + offset, data[index]);
            //            }
            //        });
            for (int i = 0; i < data.Length; i++)
            {
                if (data[i] > 1000)
                {
                    var change = data[i] / 1000;
                    data[i] %= 1000;
                    data[i] /= 2;
                    data[i] *= -1;
                }
                pls.Points[i] = new SeriesPoint(i + offset, data[i]);
            }

            chart.EndUpdate();
        }

        public void ClearChart(int chartIndex)
        {
            if (chartIndex > chart.ViewXY.PointLineSeries.Count)
                return;

            chart.BeginUpdate();
            var pls = chart.ViewXY.PointLineSeries[chartIndex];
            pls.Points = new SeriesPoint[0];

            chart.EndUpdate();
        }

        public void ClearAllCharts()
        {
            chart.BeginUpdate();
            foreach (var pls in chart.ViewXY.PointLineSeries)
                pls.Points = new SeriesPoint[0];
            chart.EndUpdate();
        }

        private void WideNarrowButton_Click(object sender, RoutedEventArgs e)
        {
            IsWideBandMode = !IsWideBandMode;
            var buttonText = IsWideBandMode ? "W" : "N";
            WideNarrowButton.Content = buttonText;
            if (IsWideBandMode)
            {
                SetConstantLineValue(0, MasterViewModel.WideBandPicThreshold);
                SetConstantLineValue(1, MasterViewModel.WideBandAttackThreshold);
                SetConstantLineValue(2, SlaveViewModel.WideBandPicThreshold);
                SetConstantLineValue(3, SlaveViewModel.WideBandAttackThreshold);
            }
            else
            {
                SetConstantLineValue(0, MasterViewModel.NarrowBandPicThreshold);
                SetConstantLineValue(1, MasterViewModel.NarrowBandAttackThreshold);
                SetConstantLineValue(2, SlaveViewModel.NarrowBandPicThreshold);
                SetConstantLineValue(3, SlaveViewModel.NarrowBandAttackThreshold);
            }
            OnVideoWorkingBandChanged?.Invoke(this, EventArgs.Empty);
        }

        private void LegendBoxToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            foreach (var box in chart.ViewXY.LegendBoxes)
                box.Visible = true;
        }

        private void LegendBoxToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            foreach (var box in chart.ViewXY.LegendBoxes)
                box.Visible = false;
        }

        private void MasterToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            if (chart == null)
                return;
            if (chart.ViewXY.ConstantLines.Count == 0)
                return;
        }

        private void MasterToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            if (chart == null)
                return;
            if (chart.ViewXY.ConstantLines.Count == 0)
                return;
        }

        private void SlaveToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            if (chart == null)
                return;
            if (chart.ViewXY.ConstantLines.Count == 0)
                return;
        }

        private void SlaveToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            if (chart == null)
                return;
            if (chart.ViewXY.ConstantLines.Count == 0)
                return;
        }

        public string MeasureTitle
        {
            get { return (string)GetValue(MeasureTitleProperty); }
            set { SetValue(MeasureTitleProperty, value); }
        }

        public static readonly DependencyProperty MeasureTitleProperty = DependencyProperty.Register(
            "MeasureTitle", typeof(string), typeof(NarrowGraph), new FrameworkPropertyMetadata("Measure", FrameworkPropertyMetadataOptions.AffectsRender));

        public string MeasureTooltip
        {
            get { return (string)GetValue(MeasureTooltipProperty); }
            set { SetValue(MeasureTooltipProperty, value); }
        }

        public static readonly DependencyProperty MeasureTooltipProperty = DependencyProperty.Register(
            "MeasureTooltip", typeof(string), typeof(NarrowGraph), new FrameworkPropertyMetadata("Narrow band measurement", FrameworkPropertyMetadataOptions.AffectsRender));

        private void MeasureButton_Checked(object sender, RoutedEventArgs e)
        {
            MeasureModeChanged?.Invoke(this, true);
        }

        private void MeasureButton_Unchecked(object sender, RoutedEventArgs e)
        {
            MeasureModeChanged?.Invoke(this, false);
        }

        public void SetMeasureMode(bool isMeasuringAllowed)
        {
            MeasureButton.IsChecked = isMeasuringAllowed;
        }

        private string AxisBase_OnFormatValueLabel(object sender, FormatValueLabelEventArgs e)
        {
            return Math.Abs(e.Value).ToString();
        }
    }
}
