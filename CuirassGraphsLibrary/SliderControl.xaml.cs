﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace CuirassGraphsLibrary
{
    /// <summary>
    /// Interaction logic for SliderControl.xaml
    /// </summary>
    public partial class SliderControl : UserControl
    {
        private SliderViewModel ViewModel => MainGrid.DataContext as SliderViewModel;

        public double Value 
        {
            get => ViewModel.Value;
            set 
            {
                if (value < Minimum || value > Maximum)
                    return;
                ViewModel.Value = value;
            }
        }

        /// <summary>
        /// This event is invoked only after mouse up event is invoked.
        /// This is a hack to invoke only last slider.OnValueChanged, but not all intermediate changes
        /// </summary>
        public event EventHandler<double> OnValueUpdated;

        public SliderControl()
        {
            InitializeComponent();

            ValueSlider.ValueChanged += ValueSlider_ValueChanged;
        }

        private void ValueSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            var valueString = SliderType == SliderControlValueType.IntSlider
                ? $"{(int)e.NewValue}"
                : $"{e.NewValue:F2}";
            ValueLabel.Content = valueString;
        }

        private void ValueSlider_PreviewMouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var value = ViewModel.Value;
            if (SliderType == SliderControlValueType.IntSlider)
                value = (int)value;
            OnValueUpdated?.Invoke(this, value);
        }

        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        public static readonly DependencyProperty TitleProperty = DependencyProperty.Register(
            "Title", typeof(string), typeof(SliderControl), new FrameworkPropertyMetadata("Title", FrameworkPropertyMetadataOptions.AffectsRender));

        public double Minimum
        {
            get { return (double)GetValue(MinimumProperty); }
            set { SetValue(MinimumProperty, value); }
        }

        public static readonly DependencyProperty MinimumProperty = DependencyProperty.Register(
            "Minimum", typeof(double), typeof(SliderControl), new FrameworkPropertyMetadata(1.0, FrameworkPropertyMetadataOptions.AffectsRender));

        public double Maximum
        {
            get { return (double)GetValue(MaximumProperty); }
            set { SetValue(MaximumProperty, value); }
        }

        public static readonly DependencyProperty MaximumProperty = DependencyProperty.Register(
            "Maximum", typeof(double), typeof(SliderControl), new FrameworkPropertyMetadata(10.0, FrameworkPropertyMetadataOptions.AffectsRender));

        public double LargeChange
        {
            get { return (double)GetValue(LargeChangeProperty); }
            set { SetValue(LargeChangeProperty, value); }
        }

        public static readonly DependencyProperty LargeChangeProperty = DependencyProperty.Register(
            "LargeChange", typeof(double), typeof(SliderControl), new FrameworkPropertyMetadata(5.0, FrameworkPropertyMetadataOptions.AffectsRender));

        public SliderControlValueType SliderType
        {
            get { return (SliderControlValueType)GetValue(SliderTypeProperty); }
            set { SetValue(SliderTypeProperty, value); }
        }

        public static readonly DependencyProperty SliderTypeProperty = DependencyProperty.Register(
            "SliderType", typeof(SliderControlValueType), typeof(SliderControl), new FrameworkPropertyMetadata(SliderControlValueType.IntSlider, FrameworkPropertyMetadataOptions.AffectsRender));
    }

    public enum SliderControlValueType 
    {
        IntSlider = 0,
        DoubleSlider = 1
    }
}
