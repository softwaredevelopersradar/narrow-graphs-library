﻿using System;

namespace CuirassGraphsLibrary
{
    public struct TimeDirectionSignalShot
    {
        public float Direction { get; }
        public DateTime DirectionTimeStamp { get; }

        public int XAxisApproximateValue { get; }

        public TimeDirectionSignalShot(float direction, int xAxisApproximateValue) 
        {
            Direction = direction;
            DirectionTimeStamp = DateTime.Now;
            XAxisApproximateValue = xAxisApproximateValue;
        }
    }
}
