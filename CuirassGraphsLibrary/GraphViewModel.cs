﻿using System.ComponentModel;

namespace CuirassGraphsLibrary
{
    public class GraphViewModel
    {
        private bool _isMasterChecked;
        public bool IsMasterChecked
        {
            get => _isMasterChecked;
            set
            {
                if (_isMasterChecked == value)
                    return;

                _isMasterChecked = value;
                OnPropertyChanged(nameof(IsMasterChecked));
            }
        }

        private bool _isSlaveChecked;
        public bool IsSlaveChecked
        {
            get => _isSlaveChecked;
            set
            {
                if (_isSlaveChecked == value)
                    return;

                _isSlaveChecked = value;
                OnPropertyChanged(nameof(IsSlaveChecked));
            }
        }

        private BandMode _graphBandMode;
        public BandMode GraphBandMode
        {
            get => _graphBandMode;
            set
            {
                if (_graphBandMode == value)
                    return;

                _graphBandMode = value;
                OnPropertyChanged(nameof(GraphBandMode));
            }
        }

        public GraphViewModel()
        {
            IsMasterChecked = true;
            IsSlaveChecked = false;
            GraphBandMode = BandMode.Wideband;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
