﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;

namespace CuirassGraphsLibrary
{
    /// <summary>
    /// Interaction logic for FrequencyAntennaSetting.xaml
    /// </summary>
    public partial class FrequencyAntennaSetting : UserControl
    {
        private SliderViewModel ViewModel => MainGrid.DataContext as SliderViewModel;
        private const int _minFrequencyMhz = 100;
        private const int _maxFrequencyMhz = 18_000;
        private const int _validationDelaySec = 5;
        private int _frequencyMhzValue;

        private readonly Stopwatch validationWatch;

        public event EventHandler<FrequencyAntennaSetup> SettingsChanged;
        public FrequencyAntennaSetting()
        {
            InitializeComponent();

            ViewModel.Value = _minFrequencyMhz;
            _frequencyMhzValue = _minFrequencyMhz;
            validationWatch = new Stopwatch();

            AntennaControl.AntennaChanged += AntennaSetupChanged;
        }

        public void TurnOn(IEnumerable<int> antennaNumbers)
        {
            AntennaControl.TurnOn(antennaNumbers);
        }

        public int GetFrequencyMhzValue() 
        {
            return _frequencyMhzValue;
        }

        public IReadOnlyList<int> GetEnabledAntennas()
        {
            return AntennaControl.GetEnabledAntennas();
        }

        private void AntennaSetupChanged(object sender, IReadOnlyList<int> enabledAntennas)
        {
            var setup = new FrequencyAntennaSetup((int)ViewModel.Value ,enabledAntennas);
            _frequencyMhzValue = setup.FrequencyMhz;
            SettingsChanged?.Invoke(this, setup);
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                var newValue = int.Parse(FrequencyTextBox.Text);
                if (ViewModel.Value == newValue)
                    return;

                if (newValue < _minFrequencyMhz || newValue > _maxFrequencyMhz)
                {
                    Task.Run(ValidationTask);
                    return;
                }

                ViewModel.Value = newValue;
                AntennaSetupChanged(this, AntennaControl.GetEnabledAntennas());
            }
            catch
            {
                //wrong input, clear
                FrequencyTextBox.Text = ViewModel.Value.ToString();
            }
        }


        /// <summary>
        /// This task is used to allow inputs out of range, but fixes them after some amount of time
        /// </summary>
        private async Task ValidationTask()
        {
            if (validationWatch.IsRunning)
            {
                validationWatch.Restart();
                return;
            }
            else
                validationWatch.Restart();
            while (validationWatch.Elapsed.TotalSeconds < _validationDelaySec)
            {
                await Task.Delay(10).ConfigureAwait(false);
            }
            validationWatch.Stop();
            Dispatcher?.Invoke(() => FrequencyTextBox.Text = ViewModel.Value.ToString());
        }

        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        public static readonly DependencyProperty TitleProperty = DependencyProperty.Register(
            "Title", typeof(string), typeof(FrequencyAntennaSetting), new FrameworkPropertyMetadata("Title", FrameworkPropertyMetadataOptions.AffectsRender));

        private void ConfirmButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (!(sender is Button button)) { return; }

            this.ValidateTextBox();
        }

        private void ValidateTextBox()
        {
            if (this.FrequencyTextBox.Text == string.Empty)
            {
                return;
            }

            if (!int.TryParse(this.FrequencyTextBox.Text, out var result))
            {
                return;
            }

            if (result < _minFrequencyMhz || result > _maxFrequencyMhz)
            {
                return;
            }

            this.ViewModel.Value = result;


            this.AntennaSetupChanged(this, this.AntennaControl.GetEnabledAntennas());
        }

        private void FrequencyTextBox_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                this.ValidateTextBox();
            }
        }
    }
}
