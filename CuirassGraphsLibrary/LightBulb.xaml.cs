﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace CuirassGraphsLibrary
{
    /// <summary>
    /// Interaction logic for LightBulb.xaml
    /// </summary>
    public partial class LightBulb : UserControl
    {
        public LightBulbState State { get; private set; } = LightBulbState.Enabled;
        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        public static readonly DependencyProperty TitleProperty = DependencyProperty.Register(
            "Title", typeof(string), typeof(LightBulb), new FrameworkPropertyMetadata("Title", FrameworkPropertyMetadataOptions.AffectsRender));

        public event EventHandler StateChanged;

        public LightBulb()
        {
            InitializeComponent();
        }

        public void TurnOn() 
        {
            if (State != LightBulbState.Enabled)
                return;

            ChangeColor(AntennaColors.WorkingAntennaColor);
            State = LightBulbState.Working;
        }

        public void TurnOff() 
        {
            if (State != LightBulbState.Working)
                return;

            ChangeColor(AntennaColors.AntennaEnabledColor);
            State = LightBulbState.Enabled;
        }

        public void ChangeState() 
        {
            if (State == LightBulbState.Enabled || State == LightBulbState.Working)
            {
                State = LightBulbState.Disabled;
                ChangeColor(AntennaColors.AntennaDisabledColor);
            }
            else
            {
                State = LightBulbState.Enabled;
                ChangeColor(AntennaColors.AntennaEnabledColor);
            }

            StateChanged?.Invoke(this, EventArgs.Empty);
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ChangeState();
        }

        private void ChangeColor(Color color)
        {
            AntennaEllipse.Fill = new SolidColorBrush(color);
        }
    }
}
