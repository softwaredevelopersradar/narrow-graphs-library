﻿using Arction.Wpf.Charting.SeriesXY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Arction.Wpf.Charting;
using Arction.Wpf.Charting.Axes;

namespace CuirassGraphsLibrary
{
    /// <summary>
    /// Interaction logic for DirectionTimePanorama.xaml
    /// </summary>
    public partial class DirectionTimePanorama : UserControl
    {
        private int _minX = 0;
        private int _maxX = 500;
        private int _minY = 0;
        private int _maxY = 360;
        private const int _maxNumberOfPoints = 100;
        private const int _maxNumberOfSignals = 100;

        private readonly Dictionary<int, IList<TimeDirectionSignalShot>> _signalShots;
        private readonly Dictionary<int, int> _signalXIndecies;
        private readonly object _lockObject;
        private readonly PointLineSeries _chartPointLineSeries;

        private int _currentlyDrawnSignalId = -1;

        public DirectionTimePanorama()
        {
            InitializeComponent();
            InitializeChart();

            _lockObject = new object();
            _signalShots = new Dictionary<int, IList<TimeDirectionSignalShot>>(_maxNumberOfSignals);//approximate max value of 
            _signalXIndecies = new Dictionary<int, int>(_maxNumberOfSignals);
            _chartPointLineSeries = Chart.ViewXY.PointLineSeries.First();
        }

        private void InitializeChart() 
        {
            var pls = new PointLineSeries
            {
                PointsVisible = true
            };
            pls.LineStyle.Color = Color.FromArgb(0xFF, 0x97, 0xD8, 0xDE);

            var color = Color.FromArgb(0xFF, 0xFF, 0x4A, 0x03);
            pls.PointStyle.Color1 = color;
            pls.PointStyle.Color3 = color;

            Chart.ViewXY.PointLineSeries.Add(pls);

            Chart.ViewXY.LegendBoxes[0].Visible = false;
            Chart.ViewXY.XAxes[0].SetRange(_minX, _maxX);
            Chart.ViewXY.YAxes[0].SetRange(_minY, _maxY);

            Chart.Title.Visible = false;
            Chart.ViewXY.XAxes[0].Visible = false;
            Chart.ViewXY.YAxes[0].Title.Visible = false;

            Chart.ViewXY.XAxes[0].RangeChanged += XAxeRangeChanged;
            Chart.ViewXY.YAxes[0].RangeChanged += YAxeRangeChanged;

            Chart.ViewXY.YAxes[0].AxisColor = Color.FromArgb(0xFF, 0x97, 0xD8, 0xDE);
            Chart.ViewXY.YAxes[0].AxisThickness = 1;
            Chart.ViewXY.Border.Color = Colors.Transparent;

            Chart.ViewXY.YAxes[0].MouseScaling = false;
            Chart.ViewXY.XAxes[0].MouseScaling = false;
        }

        private void XAxeRangeChanged(object sender, RangeChangedEventArgs e)
        {
            if (e.Axis.Maximum == _maxX && e.Axis.Minimum == _minX)
                return;
            if (e.NewMax > _maxX)
                e.NewMax = _maxX;
            if (e.NewMin < _minX)
                e.NewMin = _minX;
            e.Axis.SetRange(e.NewMin, e.NewMax);
        }

        private void YAxeRangeChanged(object sender, RangeChangedEventArgs e)
        {
            if (e.Axis.Maximum == _maxY && e.Axis.Minimum == _minY)
                return;
            if (e.NewMax > _maxY)
                e.NewMax = _maxY;
            if (e.NewMin < _minY)
                e.NewMin = _minY;
            e.Axis.SetRange(e.NewMin, e.NewMax);
        }

        public void AddSignalDirection(int signalId, float signalDirection) 
        {
            AddSignalDirectionToDictionaries(signalId, signalDirection);
            DrawChart(signalId); //todo : remove, for tests only
        }

        private void AddSignalDirectionToDictionaries(int signalId, float signalDirection)
        {
            lock (_lockObject)
            {
                var keyExists = _signalShots.ContainsKey(signalId);
                if (!keyExists)
                {
                    CreateStorageAndIndexerForSignal(signalId);
                    AddSignalValues(signalId, signalDirection);
                    return;
                }

                if (_signalShots[signalId].Count == _maxNumberOfPoints)
                    _signalShots[signalId].RemoveAt(0);
                AddSignalValues(signalId, signalDirection);
            }

            void AddSignalValues(int id, float direction)
            {
                _signalShots[id].Add(new TimeDirectionSignalShot(direction, _signalXIndecies[signalId]));
                _signalXIndecies[id]++;
            }

            void CreateStorageAndIndexerForSignal(int id)
            {
                _signalShots.Add(id, new List<TimeDirectionSignalShot>(_maxNumberOfPoints));
                _signalXIndecies.Add(id, 1);
            }
        }

        public void AddSignalsWithDirections(IDictionary<int, float> signalsIdDirectionDictionary) { }

        public void ClearAllSignalDirections() { }
        public void ClearSignalDirections(int signalId)
        {
            lock (_lockObject) 
            {
                _signalShots.Remove(signalId);
                if (_currentlyDrawnSignalId == signalId)
                    ClearChart();
            }
        }

        /// <summary>
        /// Redraws chart for selected signal
        /// </summary>
        public void DrawChart(int signalId) 
        {
            if (!_signalShots.ContainsKey(signalId))
                return;
            Chart.BeginUpdate();

            var signalDirections = _signalShots[signalId].ToList();
            _chartPointLineSeries.Points = new SeriesPoint[signalDirections.Count];
            
            for (int i = 0; i < signalDirections.Count; i++)
            {
                _chartPointLineSeries.Points[i] = new SeriesPoint(signalDirections[i].XAxisApproximateValue, signalDirections[i].Direction);
            }

            Chart.ViewXY.ZoomToFit();

            Chart.EndUpdate();
            _currentlyDrawnSignalId = signalId;
        }

        public void ClearChart()
        {
            Chart.BeginUpdate();
            _chartPointLineSeries.Points = new SeriesPoint[_maxNumberOfPoints];
            Chart.EndUpdate();
            _currentlyDrawnSignalId = -1;
        }
    }
}
