﻿using System;

namespace CuirassGraphsLibrary
{
    public class VideoGraphModel : ICloneable
    {
        public FtmDevice Device { get; set; }
        public BandMode Mode { get; set; }

        public DeviceThresholds MasterThresholds { get; set; }
        public DeviceThresholds SlaveThresholds { get; set; }

        public VideoGraphModel() 
        {
            MasterThresholds = new DeviceThresholds();
            SlaveThresholds = new DeviceThresholds();
        }

        public object Clone()
        {
            return new VideoGraphModel() 
            {
                Device = this.Device,
                Mode = this.Mode,
                MasterThresholds = (DeviceThresholds)this.MasterThresholds.Clone(),
                SlaveThresholds = (DeviceThresholds)this.SlaveThresholds.Clone()
            };
        }
    }

    public class VideoThresholds : ICloneable
    {
        private int _attackThreshold;
        private int _picThreshold;
        public int AttackThreshold 
        {
            get => _attackThreshold;
            set 
            {
                if (value >= 1)
                    _attackThreshold = value;
                else
                    _attackThreshold = 1;
            }
        }
        public int PicThreshold
        {
            get => _picThreshold;
            set
            {
                _picThreshold = value;
                if (_picThreshold < 1)
                    _picThreshold = 1;
            }
        }

        public object Clone()
        {
            return new VideoThresholds()
            {
                AttackThreshold = this.AttackThreshold,
                PicThreshold = this.PicThreshold
            };
        }
    }

    public class DeviceThresholds : ICloneable
    {
        public VideoThresholds WideThresholds { get; set; }
        public VideoThresholds NarrowThresholds { get; set; }

        public DeviceThresholds() 
        {
            WideThresholds = new VideoThresholds();
            NarrowThresholds = new VideoThresholds();
        }

        public object Clone()
        {
            return new DeviceThresholds()
            {
                WideThresholds = (VideoThresholds)this.WideThresholds.Clone(),
                NarrowThresholds = (VideoThresholds)this.NarrowThresholds.Clone()
            };
        }
    }
}
