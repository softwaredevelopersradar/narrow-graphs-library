﻿using System.ComponentModel;

namespace CuirassGraphsLibrary
{
    public class ThresholdViewModel : INotifyPropertyChanged
    {
        private int _wideBandPicThreshold;
        public int WideBandPicThreshold
        {
            get => _wideBandPicThreshold;
            set
            {
                if (_wideBandPicThreshold == value)
                    return;

                _wideBandPicThreshold = value;
                OnPropertyChanged(nameof(WideBandPicThreshold));
            }
        }
        private int _narrowBandPicThreshold;
        public int NarrowBandPicThreshold
        {
            get => _narrowBandPicThreshold;
            set
            {
                if (_narrowBandPicThreshold == value)
                    return;

                _narrowBandPicThreshold = value;
                OnPropertyChanged(nameof(NarrowBandPicThreshold));
            }
        }

        private int _wideBandAttackThreshold;
        public int WideBandAttackThreshold
        {
            get => _wideBandAttackThreshold;
            set
            {
                if (_wideBandAttackThreshold == value)
                    return;

                _wideBandAttackThreshold = value;
                OnPropertyChanged(nameof(WideBandAttackThreshold));
            }
        }
        private int _narrowBandAttackThreshold;
        public int NarrowBandAttackThreshold
        {
            get => _narrowBandAttackThreshold;
            set
            {
                if (_narrowBandAttackThreshold == value)
                    return;

                _narrowBandAttackThreshold = value;
                OnPropertyChanged(nameof(NarrowBandAttackThreshold));
            }
        }

        public ThresholdViewModel()
        {
            WideBandPicThreshold = 0;
            NarrowBandPicThreshold = 0;

            WideBandAttackThreshold = 0;
            NarrowBandAttackThreshold = 0;
            OnPropertyChanged(nameof(WideBandPicThreshold));
            OnPropertyChanged(nameof(NarrowBandPicThreshold));
            OnPropertyChanged(nameof(WideBandAttackThreshold));
            OnPropertyChanged(nameof(NarrowBandAttackThreshold));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public ThresholdViewModel CreateCopy() => new ThresholdViewModel
        {
            WideBandAttackThreshold = this.WideBandAttackThreshold,
            WideBandPicThreshold = this.WideBandPicThreshold,
            NarrowBandAttackThreshold = this.NarrowBandAttackThreshold,
            NarrowBandPicThreshold = this.NarrowBandPicThreshold
        };
    }
}
