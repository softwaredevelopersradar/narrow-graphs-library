﻿using System.Windows.Media;

namespace CuirassGraphsLibrary
{
    public enum FtmDevice : byte
    {
        Master = 0,
        Slave = 1
    }

    public enum BandMode : byte
    {
        Wideband,
        Narrowband
    }

    public enum LightBulbState : byte
    {
        Disabled = 0,
        Enabled = 1,
        Working = 2
    }

    public static class AntennaColors
    {
        public static Color WorkingAntennaColor = Color.FromArgb(0xFF, 0x97, 0xD8, 0xDE);
        public static Color AntennaEnabledColor = Colors.Azure;
        public static Color AntennaDisabledColor = Color.FromArgb(0x10, 0x97, 0xD8, 0xDE);
    }
}
