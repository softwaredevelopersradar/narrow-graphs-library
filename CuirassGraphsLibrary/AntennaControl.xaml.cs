﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Collections.Generic;

namespace CuirassGraphsLibrary
{
    /// <summary>
    /// Interaction logic for AntennaControl.xaml
    /// </summary>
    public partial class AntennaControl : UserControl
    {
        private readonly List<LightBulb> _lightBulbs;

        public event EventHandler<IReadOnlyList<int>> AntennaChanged;

        public AntennaControl()
        {
            InitializeComponent();

            _lightBulbs = AntennasGrid.Children.Cast<LightBulb>().ToList();
            SubscribeToAntennaChanges();
        }

        private void AntennaControl_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            foreach (var bulb in _lightBulbs)
            {
                if (bulb.State == LightBulbState.Disabled)
                    bulb.ChangeState();
            }
        }

        private void SubscribeToAntennaChanges() 
        {
            foreach (var bulb in _lightBulbs)
            {
                bulb.StateChanged += AntennaControl_StateChanged;
            }
        }

        private void AntennaControl_StateChanged(object sender, EventArgs e)
        {
            var enabledAntennas = GetEnabledAntennas();
            var masterEnabledCount = enabledAntennas.Where(antennaNumber => antennaNumber % 2 == 1).Count();
            var slaveEnabledCount = enabledAntennas.Where(antennaNumber => antennaNumber % 2 == 0).Count();
            if (masterEnabledCount == 0 || slaveEnabledCount == 0 ||
                masterEnabledCount + slaveEnabledCount < 2)
            {                
                var lightBulb = sender as LightBulb;
                lightBulb.ChangeState();
                return;
            }
            AntennaChanged?.Invoke(this, enabledAntennas);
        }

        public void TurnOn(IEnumerable<int> antennaNumbers)
        {
            for(int i = 0; i < _lightBulbs.Count; i++)
            {
                if (antennaNumbers.Contains(i + 1)) // convert antenna number to index
                    _lightBulbs[i].TurnOn();
                else
                    _lightBulbs[i].TurnOff();
            }
        }

        public IReadOnlyList<int> GetEnabledAntennas()
        {
            var output = new List<int>(_lightBulbs.Count);
            for (int i = 0; i < _lightBulbs.Count; i++)
            {
                if (_lightBulbs[i].State == LightBulbState.Enabled 
                    || _lightBulbs[i].State == LightBulbState.Working)
                    output.Add(i + 1);
            }
            return output;
        }
    }
}
